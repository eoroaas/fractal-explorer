package com.idatt2003.fractalexplorer.model.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexTest {

  Complex complex = new Complex(1, 1);

  @Test
  void subtract() {
    Complex complex2 = new Complex(1, 1);
    Complex result = complex.subtract(complex2);
    assertEquals(0, result.getRealPart());
    assertEquals(0, result.getImaginaryPart());
  }

  @Test
  void testSubtract() {
    Complex result = complex.subtract(new Vector2D(1, 1));
    assertEquals(0, result.getRealPart());
    assertEquals(0, result.getImaginaryPart());

  }

  @Test
  void negate() {
    Complex result = complex.negate();
    assertEquals(-1, result.getRealPart());
    assertEquals(-1, result.getImaginaryPart());
  }

  @Test
  void sqrt() {
    Complex[] result = complex.sqrt();
    assertEquals(String.format("%.5g%n", 1.09868411346781), String.format("%.5g%n", result[0].getRealPart()));
    assertEquals(String.format("%.5g%n", 0.45508986056222733), String.format("%.5g%n", result[0].getImaginaryPart()));
  }

  @Test
  void getRealPart() {
    assertEquals(1, complex.getRealPart());
  }

  @Test
  void setRealPart() {
    complex.setRealPart(2);
    assertEquals(2, complex.getRealPart());
  }

  @Test
  void getImaginaryPart() {
    assertEquals(1, complex.getImaginaryPart());
  }

  @Test
  void setImaginaryPart() {
    complex.setImaginaryPart(2);
    assertEquals(2, complex.getImaginaryPart());

  }
}