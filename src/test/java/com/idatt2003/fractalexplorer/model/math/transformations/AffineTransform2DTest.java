package com.idatt2003.fractalexplorer.model.math.transformations;

import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AffineTransform2DTest {
  AffineTransform2D affineTransform2D = new AffineTransform2D(new Matrix2x2(0, 1, 0, 1), new Vector2D(0, 0));

  @Test
  void transform() {
    assertEquals(new Vector2D(0, 0).toString(), affineTransform2D.transform(new Vector2D(0, 0)).toString());

  }

  @Test
  void getMatrix() {
    assertEquals(new Matrix2x2(0, 1, 0, 1).toString(), affineTransform2D.getMatrix().toString());
  }

  @Test
  void getVector() {
    assertEquals(new Vector2D(0, 0).toString(), affineTransform2D.getVector().toString());
  }
}