package com.idatt2003.fractalexplorer.model.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Matrix2x2Test {

  @Test
  void multiply() {
    Matrix2x2 matrix = new Matrix2x2(1, 1, 1, 1);
    Vector2D result = matrix.multiply(new Vector2D(1, 1));
    assertEquals(2, result.getX0());
    assertEquals(2, result.getX1());

  }

  @Test
  void testToString() {
    Matrix2x2 matrix = new Matrix2x2(1, 1, 1, 1);
    assertEquals("Matrix2x2{a00=1.0, a01=1.0, a10=1.0, a11=1.0}", matrix.toString());
  }
}