package com.idatt2003.fractalexplorer.model.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {

  @Test
  void add() {
    Vector2D vector = new Vector2D(1, 1);
    Vector2D result = vector.add(new Vector2D(1, 1));
    assertEquals(2, result.getX0());
    assertEquals(2, result.getX1());
  }

  @Test
  void getX0() {
    Vector2D vector = new Vector2D(1, 1);
    assertEquals(1, vector.getX0());
  }

  @Test
  void getX1() {
    Vector2D vector = new Vector2D(1, 1);
    assertEquals(1, vector.getX1());
  }

  @Test
  void subtract() {
    Vector2D vector = new Vector2D(1, 1);
    Vector2D result = vector.subtract(new Vector2D(1, 1));
    assertEquals(0, result.getX0());
    assertEquals(0, result.getX1());
  }

  @Test
  void scale() {
    Vector2D vector = new Vector2D(1, 1);
    Vector2D result = vector.scale(2);
    assertEquals(2, result.getX0());
    assertEquals(2, result.getX1());
  }

  @Test
  void testToString() {
    Vector2D vector = new Vector2D(1, 1);
    assertEquals("Vector2D{x0=1.0, x1=1.0}", vector.toString());
  }
}