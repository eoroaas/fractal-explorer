package com.idatt2003.fractalexplorer.model.math.transformations;

import com.idatt2003.fractalexplorer.model.math.Complex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JuliaTransformTest {

  @Test
  void transform() {
    JuliaTransform juliaTransform = new JuliaTransform(new Complex(0, 0), 0);
    assertEquals(new Complex(-0, -0).toString(), juliaTransform.transform(new Complex(0, 0)).toString());
  }
}