package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameTest {

  ChaosGame chaosGame = new ChaosGame(new ChaosGameDescription(new Vector2D(0, 0), new Vector2D(1, 1),
      List.of(
          new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
          new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(1, 0)),
          new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0.5))
      )), 100, 100, FractalName.SIERPINSKI);

  @Test
  void setWidth() {
    chaosGame.setWidth(200);
    assertEquals(200, chaosGame.getWidth());
  }

  @Test
  void getScale() {
    assertEquals(1, chaosGame.getScale());

  }

  @Test
  void setScale() {
    chaosGame.setScale(2);
    assertEquals(2, chaosGame.getScale());
  }

  @Test
  void getSteps() {
    assertEquals(10000, chaosGame.getSteps());
  }

  @Test
  void setSteps() {
    chaosGame.setSteps(200);
    assertEquals(200, chaosGame.getSteps());
  }

  @Test
  void setHeight() {
    chaosGame.setHeight(200);
    assertEquals(200, chaosGame.getHeight());

  }

  @Test
  void getCanvas() {
    assertNotNull(chaosGame.getCanvas());
  }

  @Test
  void getCurrentFractalName() {
    assertEquals(FractalName.SIERPINSKI, chaosGame.getCurrentFractalName());
  }

  @Test
  void setCurrentFractalName() {
    chaosGame.setCurrentFractalName(FractalName.BARNSLEY);
    assertEquals(FractalName.BARNSLEY, chaosGame.getCurrentFractalName());
  }

  @Test
  void getDescription() {
    assertNotNull(chaosGame.getDescription());
  }

  @Test
  void setDescription() {
    ChaosGameDescription chaosGameDescription = new ChaosGameDescription(new Vector2D(0, 0), new Vector2D(1, 1),
        List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(1, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0.5))
        ));
    chaosGame.setDescription(chaosGameDescription);
    assertEquals(chaosGameDescription, chaosGame.getDescription());
  }

  @Test
  void setOffset() {
    chaosGame.setOffset(1, 1);
    assertEquals(1, chaosGame.getOffsetX());
    assertEquals(1, chaosGame.getOffsetY());
  }

  @Test
  void runSteps() {
    chaosGame.runSteps();
    assertNotNull(chaosGame.getCanvas());
  }

  @Test
  void setFractalName() {
    chaosGame.setFractalName(FractalName.BARNSLEY);
    assertEquals(FractalName.BARNSLEY, chaosGame.getCurrentFractalName());
  }
}