package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.model.math.Vector2D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <h2>Chaos Game Canvas Test</h2>
 * <p>Test class for the Chaos Game Canvas</p>
 */
class ChaosGameCanvasTest {
  ChaosGameCanvas chaosGameCanvas = new ChaosGameCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1), 0, 0);

  @Test
  void getOffsetY() {
    assertEquals(0, chaosGameCanvas.getOffsetY());
    assertNotEquals(10, chaosGameCanvas.getOffsetY());

  }

  @Test
  void setOffsetY() {
    chaosGameCanvas.setOffsetY(20);
    assertEquals(20, chaosGameCanvas.getOffsetY());
  }

  @Test
  void getOffsetX() {
    assertEquals(0, chaosGameCanvas.getOffsetX());
    assertNotEquals(10, chaosGameCanvas.getOffsetX());
  }

  @Test
  void setOffsetX() {
    chaosGameCanvas.setOffsetX(20);
    assertEquals(20, chaosGameCanvas.getOffsetX());
  }

  @Test
  void clearCanvas() {
    chaosGameCanvas.putPixel(new Vector2D(0, 0));
    chaosGameCanvas.clearCanvas();
    assertEquals(0, chaosGameCanvas.getCanvasArray()[0][0]);

  }

  @Test
  void getCanvasArray() {
    assertEquals(100, chaosGameCanvas.getCanvasArray().length);
    assertEquals(100, chaosGameCanvas.getCanvasArray()[0].length);
  }

  @Test
  void putPixel() {
    assertTrue(chaosGameCanvas.putPixel(new Vector2D(0, 0)));
    assertFalse(chaosGameCanvas.putPixel(new Vector2D(101, 101)));
  }
}