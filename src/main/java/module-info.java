module com.idatt2003.fractalexplorer {
  requires javafx.controls;
  requires javafx.fxml;
  requires org.girod.javafx.svgimage;
  requires javafx.graphics;
  opens com.idatt2003.fractalexplorer to javafx.fxml;
  exports com.idatt2003.fractalexplorer;
  exports com.idatt2003.fractalexplorer.controller;
  exports com.idatt2003.fractalexplorer.view.components;
  exports com.idatt2003.fractalexplorer.view.pages;
  exports com.idatt2003.fractalexplorer.model;
}