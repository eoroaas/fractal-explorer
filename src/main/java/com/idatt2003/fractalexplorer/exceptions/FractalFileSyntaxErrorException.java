package com.idatt2003.fractalexplorer.exceptions;

/**
 * <h2>Fractal file syntax error exception</h2>
 * <p>Exception thrown when the syntax of the fractal file is incorrect.</p>
 */
public class FractalFileSyntaxErrorException extends Exception {

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new FractalFileSyntaxErrorException with the specified message.</p>
   *
   * @param message The message to be displayed when the exception is thrown.
   */
  public FractalFileSyntaxErrorException(String message) {
    super(message);
  }
}
