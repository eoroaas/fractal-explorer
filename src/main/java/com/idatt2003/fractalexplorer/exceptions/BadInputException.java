package com.idatt2003.fractalexplorer.exceptions;

/**
 * <h2>Bad input exception</h2>
 * <p>Exception thrown when the input is invalid.</p>
 */
public class BadInputException extends Exception {

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new BadInputException with the specified message.</p>
   *
   * @param message The message to be displayed when the exception is thrown.
   */
  public BadInputException(String message) {
    super(message);
  }
}
