package com.idatt2003.fractalexplorer.controller;

import com.idatt2003.fractalexplorer.view.MainView;

/**
 * <h2>Navigation controller</h2>
 * <p>
 * Controller for the navigation between pages.
 * Handles the logic for navigating between the main page, settings page, export page and import page.
 * </p
 */
public class NavigationController {

  private final MainView mainView;

  /**
   * <h2>Constructor</h2>
   * <p>The navigation controller constructor.</p>
   *
   * @param mainView The main view.
   */
  public NavigationController(MainView mainView) {
    this.mainView = mainView;
    mainView.settingsModalPage.setOnExitAction(this::goToMainPage);
    mainView.exportModalPage.setOnExitAction(this::goToMainPage);
    mainView.importModalPage.setOnExitAction(this::goToMainPage);
    mainView.mainPage.getNavbar().settingsButton.setOnAction(e -> {
      goToSettingsPage();
    });

    mainView.mainPage.getNavbar().exportButton.setOnAction(e -> {
      goToExportPage();
    });

    mainView.mainPage.getNavbar().importButton.setOnAction(e -> {
      goToImportPage();
    });

    mainView.exportModalPage.setOnExitAction(this::goToMainPage);

  }

  /**
   * <h2>Go to main page</h2>
   * <p>
   * Navigates to the main page.
   * </p>
   */
  public void goToMainPage() {
    mainView.mainPage.toFront();
  }

  /**
   * <h2>Go to settings page</h2>
   * <p>
   * Navigates to the settings page.
   * </p>
   */
  public void goToSettingsPage() {
    mainView.settingsModalPage.toFront();
  }

  /**
   * <h2>Go to export page</h2>
   * <p>
   * Navigates to the export page.
   * </p>
   */
  public void goToExportPage() {
    mainView.exportModalPage.toFront();
  }

  /**
   * <h2>Go to import page</h2>
   * <p>
   * Navigates to the import page.
   * </p>
   */
  public void goToImportPage() {
    mainView.importModalPage.toFront();
  }
}
