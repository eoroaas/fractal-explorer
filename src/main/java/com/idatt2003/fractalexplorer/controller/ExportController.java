package com.idatt2003.fractalexplorer.controller;

import com.idatt2003.fractalexplorer.MainApplication;
import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.exceptions.BadInputException;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.model.ChaosGameDescription;
import com.idatt2003.fractalexplorer.model.math.Complex;
import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;
import com.idatt2003.fractalexplorer.model.math.transformations.JuliaTransform;
import com.idatt2003.fractalexplorer.model.math.transformations.Transform2D;
import com.idatt2003.fractalexplorer.view.components.TextInput;
import com.idatt2003.fractalexplorer.view.pages.ExportModalPage;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * <h2>The export controller</h2>
 * <p>
 * Controller for the export modal page.
 * Handles the logic for exporting a fractal to a file and creating a new custom fractal.
 * </p>
 */
public class ExportController {

  /**
   * The export modal page. (View)
   */
  private final ExportModalPage page;

  /**
   * <h2>Constructor</h2>
   * <p>The export controller constructor.</p>
   *
   * @param page      The export modal page.
   * @param chaosGame The chaos game model.
   */
  public ExportController(ExportModalPage page, ChaosGame chaosGame) {
    this.page = page;

    page.setOnApply((e) -> {
      if (page.getFractalType().getSelected() == null || page.getFractalType().getSelected().isEmpty()) {
        page.getFractalType().setError("Please select a fractal type");
        return;
      } else {
        page.getFractalType().removeError();
      }
      Double minX;
      Double minY;
      Double maxX;
      Double maxY;
      try {
        minX = page.getMinX().tryGetDouble();
        minY = page.getMinY().tryGetDouble();
        maxX = page.getMaxX().tryGetDouble();
        maxY = page.getMaxY().tryGetDouble();
      } catch (Exception exc) {
        MainApplication.logger.log(System.Logger.Level.INFO, "Bad input in min max vector form.");
        exc.printStackTrace();
        return;
      }

      Vector2D min = new Vector2D(minX, minY);
      Vector2D max = new Vector2D(maxX, maxY);

      if (page.getFractalType().getSelected().equals("Julia")) {
        Complex c = new Complex(
            Double.parseDouble(page.getJuliaForm().getRealPart().getValue()),
            Double.parseDouble(page.getJuliaForm().getImaginaryPart().getValue())
        );
        chaosGame.setDescription(new ChaosGameDescription(
            min,
            max,
            List.of(
                new JuliaTransform(c, 1),
                new JuliaTransform(c, -1)
            )
        ));
      } else {
        List<Transform2D> transforms = new ArrayList<>();
        page.getAffineForm().getRows().getChildren().forEach(row -> {
          double a00;
          double a01;
          double a10;
          double a11;
          double b0;
          double b1;

          try {
            a00 = (((TextInput) ((HBox) row).getChildren().get(0)).tryGetDouble());
            a01 = (((TextInput) ((HBox) row).getChildren().get(1)).tryGetDouble());
            a10 = (((TextInput) ((HBox) row).getChildren().get(2)).tryGetDouble());
            a11 = (((TextInput) ((HBox) row).getChildren().get(3)).tryGetDouble());
            b0 = (((TextInput) ((HBox) row).getChildren().get(4)).tryGetDouble());
            b1 = (((TextInput) ((HBox) row).getChildren().get(5)).tryGetDouble());
          } catch (BadInputException exc) {
            MainApplication.logger.log(System.Logger.Level.INFO, "Bad input in affine form.");
            exc.printStackTrace();
            return;
          }
          Vector2D v = new Vector2D(
              b0,
              b1
          );
          Matrix2x2 m = new Matrix2x2(
              a00, a01,
              a10, a11
          );
          transforms.add(new AffineTransform2D(m, v));
        });
        chaosGame.setDescription(new ChaosGameDescription(
            min,
            max,
            transforms
        ));
      }
      chaosGame.setFractalName(FractalName.CUSTOM);
      page.hide();
    });

    page.setOnExport((e) -> {
      exportToFile();
      page.hide();
    });

  }

  /**
   * <h2>Export to file</h2>
   * <p>
   * Exports the fractal description to a file.
   * The file is a text file with the following format:
   * </p>
   * <ul>
   *   <li>Fractal type</li>
   *   <li>Min vector</li>
   *   <li>Max vector</li>
   *   <li>Julia constant (if fractal type is Julia)</li>
   *   <li>Affine tranformations (if fractal type is Affine)</li>
   *   </ul>
   */
  private void exportToFile() {
    // Export to file
    StringBuilder sb = new StringBuilder();
    sb.append(page.getFractalType().getSelected()).append("\n");
    sb.append(page.getMinX().getValue()).append(", ").append(page.getMinY().getValue()).append("\n");
    sb.append(page.getMaxX().getValue()).append(", ").append(page.getMaxY().getValue()).append("\n");

    if (page.getFractalType().getSelected().equals("Julia")) {
      sb.append(page.getJuliaForm().getRealPart().getValue()).append(", ").append(page.getJuliaForm().getImaginaryPart().getValue()).append("\n");
    } else {
      page.getAffineForm().getRows().getChildren().forEach(row -> {
        sb.append(((TextInput) ((HBox) row).getChildren().get(0)).getValue()).append(", ");
        sb.append(((TextInput) ((HBox) row).getChildren().get(1)).getValue()).append(", ");
        sb.append(((TextInput) ((HBox) row).getChildren().get(2)).getValue()).append(", ");
        sb.append(((TextInput) ((HBox) row).getChildren().get(3)).getValue()).append(", ");
        sb.append(((TextInput) ((HBox) row).getChildren().get(4)).getValue()).append(", ");
        sb.append(((TextInput) ((HBox) row).getChildren().get(5)).getValue()).append("\n");
      });
    }

    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Save Fractal Description");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    fileChooser.setInitialFileName("fractal_description.txt");
    File file = fileChooser.showSaveDialog(page.getScene().getWindow());
    try {
      if (file != null) {
        FileWriter writer = new FileWriter(file);
        writer.write(sb.toString());
        writer.close();
      }
    } catch (Exception e) {
      MainApplication.logger.log(System.Logger.Level.ERROR, "Failed to write to file.");
      MainApplication.logger.log(System.Logger.Level.ERROR, e.getMessage());
    }

  }
}
