package com.idatt2003.fractalexplorer.controller;

import com.idatt2003.fractalexplorer.MainApplication;
import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.exceptions.FractalFileSyntaxErrorException;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.model.ChaosGameDescription;
import com.idatt2003.fractalexplorer.model.math.Complex;
import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;
import com.idatt2003.fractalexplorer.model.math.transformations.JuliaTransform;
import com.idatt2003.fractalexplorer.model.math.transformations.Transform2D;
import com.idatt2003.fractalexplorer.view.pages.ImportModalPage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * <h2>The import controller</h2>
 * <p>
 * Controller for the import modal page.
 * Handles the logic for importing a fractal from a file.
 * </p>
 */
public class ImportController {

  ChaosGame chaosGame;

  /**
   * <h2>Constructor</h2>
   * <p>The import controller constructor.</p>
   *
   * @param page      The import modal page.
   * @param chaosGame The chaos game model.
   */
  public ImportController(ImportModalPage page, ChaosGame chaosGame) {
    this.chaosGame = chaosGame;
    page.setOnFileChoosen((file) -> {
      try {
        parseFractalFromFile(file);
        page.hide();
      } catch (Exception e) {
        MainApplication.logger.log(System.Logger.Level.ERROR, "Something went wrong when trying to parse the file.");
        MainApplication.logger.log(System.Logger.Level.ERROR, e.getMessage());
        throw new RuntimeException(e);
      }
    });

  }

  /**
   * <h2>Parse fractal from file</h2>
   * <p>
   * Parses a fractal from a file.
   * </p>
   *
   * @param file The file to parse.
   * @throws FileNotFoundException If the file is not found.
   */
  void parseFractalFromFile(File file) throws FileNotFoundException, FractalFileSyntaxErrorException {
    ChaosGameDescription newDescription = null;
    try (Scanner scanner = new Scanner(file)) {
      //first line is the type of the transform
      String name = cleanLine(scanner.nextLine());
      //the next line is the min vector
      //remove all commas and split the string into an array of integers
      Double[] minVectorVals = parseNumbersFromLine(cleanLine(scanner.nextLine()));
      Vector2D min = new Vector2D(minVectorVals[0], minVectorVals[1]);
      //the next line is the max vector
      Double[] maxVectorVals = parseNumbersFromLine(cleanLine(scanner.nextLine()));
      Vector2D max = new Vector2D(maxVectorVals[0], maxVectorVals[1]);

      //the next lines are the transforms
      if (scanner.hasNext()) {

        if ("Affine2D".equals(name)) {
          newDescription = new ChaosGameDescription(min, max, parseAffine(scanner));
        } else if ("Julia".equals(name)) {
          newDescription = new ChaosGameDescription(min, max, parseJulia(scanner));
        } else {
          throw new IllegalArgumentException("No valid transform type found. Found: "
              + name);
        }
      } else {
        throw new NoSuchElementException("No transforms are in file");
      }
      chaosGame.setDescription(newDescription);
      chaosGame.setFractalName(FractalName.CUSTOM);
    } catch (FileNotFoundException e) {
      MainApplication.logger.log(System.Logger.Level.ERROR, "File not found when trying to parse.");
      MainApplication.logger.log(System.Logger.Level.ERROR, e.getMessage());
    } catch (NoSuchElementException e) {
      MainApplication.logger.log(System.Logger.Level.ERROR, "Bad input in affine form.");
      throw new FractalFileSyntaxErrorException("Bad input in file");
    }

  }

  /**
   * <h2>Clean line</h2>
   * <p>
   * Cleans a line from comments and trailing whitespace.
   * </p>
   *
   * @param line The line to clean.
   * @return The cleaned line.
   */
  private String cleanLine(String line) {
    if (line.contains("#")) {
      return line.substring(0, line.indexOf("#")).trim();
    } else {
      return line.trim();
    }
  }

  /**
   * <h2>Parse numbers from line</h2>
   * <p>
   * Parses numbers from a line.
   * </p>
   *
   * @param line The line to parse.
   * @return The parsed numbers.
   */
  private Double[] parseNumbersFromLine(String line) {
    System.out.println(line);
    return Arrays.stream(line.split(" ")).map(val -> Double.parseDouble(val.replaceAll(",", ""))).toArray(Double[]::new);
  }

  /**
   * <h2>Parse affine</h2>
   * <p>
   * Parses affine transformations from a scanner.
   * </p>
   *
   * @param scanner The scanner to parse from.
   * @return The parsed affine transformations.
   */
  private List<Transform2D> parseAffine(Scanner scanner) {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    while (scanner.hasNextLine()) {
      String line = cleanLine(scanner.nextLine());
      if (line.isEmpty())
        break;
      Double[] numbers = parseNumbersFromLine(line);
      if (numbers.length != 6) {
        throw new IllegalArgumentException("Invalid number of values in affine transform");
      }
      transforms.add(new AffineTransform2D(new Matrix2x2(numbers[0], numbers[1], numbers[2], numbers[3]), new Vector2D(numbers[4], numbers[5])));
      for (Double number : numbers) {
        System.out.println(number);
      }
    }
    return transforms;
  }

  /**
   * <h2>Parse julia</h2>
   * <p>
   * Parses julia transformations from a scanner.
   * </p>
   *
   * @param scanner The scanner to parse from.
   * @return The parsed julia transformations.
   */
  private List<Transform2D> parseJulia(Scanner scanner) {
    ArrayList<Transform2D> transforms = new ArrayList<>();
    //parse the complex number
    Double[] complexVals = parseNumbersFromLine(cleanLine(scanner.nextLine()));

    Complex c = new Complex(complexVals[0], complexVals[1]);

    transforms.add(new JuliaTransform(c, 1));
    transforms.add(new JuliaTransform(c, -1));

    return transforms;
  }

}
