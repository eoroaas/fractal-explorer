package com.idatt2003.fractalexplorer.controller;

import com.idatt2003.fractalexplorer.MainApplication;
import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.view.pages.SettingsModalPage;

import java.util.Arrays;

/**
 * <h2>The settings controller</h2>
 * <p>
 * Controller for the settings modal page.
 * Handles the logic for the settings page.
 * </p>
 */
public class SettingsController {

  /**
   * <h2>Constructor</h2>
   * <p>The settings controller constructor.</p>
   *
   * @param settingsModalPage The settings modal page.
   * @param chaosGame         The chaos game model.
   * @param fractalController The fractal controller.
   */
  public SettingsController(SettingsModalPage settingsModalPage, ChaosGame chaosGame, FractalController fractalController) {
    //every FractalName, except CUSTOM, is added to the select component
    settingsModalPage.getSelectFractalType().setValues(
        Arrays.stream(FractalName.values())
            .filter(name -> name != FractalName.CUSTOM)
            .map(Enum::name)
            .toArray(String[]::new)
    );

    settingsModalPage.getSelectFractalType().setOnSelect((String value) -> {
      System.out.println(value);
      FractalName fractalName = FractalName.valueOf(value);
      chaosGame.setFractalName(fractalName);
      if (fractalName == FractalName.JULIA) {
        fractalController.setFractalToJuliaSet();
      } else if (fractalName == FractalName.BARNSLEY) {
        fractalController.setFractalToBarnsleyFern();
      } else if (fractalName == FractalName.SIERPINSKI) {
        fractalController.setFractalToSierpinski();
      } else if (fractalName == FractalName.LEVY_CURVE) {
        fractalController.setFractalToLevyCurve();
      }

    });

    settingsModalPage.setOnSave(() -> {
      //first we validate the input
      String iterations = settingsModalPage.getIterationsValue();
      int steps = 0;
      if (iterations.isEmpty()) {
        settingsModalPage.getIterationsInput().setError("Iterations must be a number");
        return;
      }
      try {
        steps = Integer.parseInt(iterations);
        if (steps < 1) {
          MainApplication.logger.log(System.Logger.Level.ERROR, "Bad input in settings page, not enough iterations");

          settingsModalPage.getIterationsInput().setError("Iterations must be greater than 0");
          return;
        }
        if (steps > 500000) {
          MainApplication.logger.log(System.Logger.Level.ERROR, "Bad input in settings page, too many iterations");

          settingsModalPage.getIterationsInput().setError("Iterations must be less than 500000");
          return;
        }
      } catch (NumberFormatException e) {
        MainApplication.logger.log(System.Logger.Level.INFO, "Bad input in settings page.");
        settingsModalPage.getIterationsInput().setError("Iterations must be a number");
        return;
      }

      chaosGame.setSteps(steps);
      settingsModalPage.getIterationsInput().removeError();
      settingsModalPage.hide();
    });

  }
}
