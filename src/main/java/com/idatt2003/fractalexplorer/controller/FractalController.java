package com.idatt2003.fractalexplorer.controller;

import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.model.ChaosGameDescription;
import com.idatt2003.fractalexplorer.model.math.Complex;
import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;
import com.idatt2003.fractalexplorer.model.math.transformations.JuliaTransform;
import com.idatt2003.fractalexplorer.model.math.transformations.Transform2D;
import com.idatt2003.fractalexplorer.view.components.FractalView;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.List;

/**
 * <h2>The fractal controller</h2>
 * <p>
 * Controller for the fractal view.
 * Handles the logic for changing the fractal type and drawing the fractal.
 * </p>
 */
public class FractalController {

  int dragStartX;

  int dragStartY;

  FractalView fractalView;

  GraphicsContext graphicsContext;

  ChaosGame chaosGame;

  /**
   * <h2>Constructor</h2>
   * <p>The fractal controller constructor.</p>
   *
   * @param fractalView The fractal view.
   * @param chaosGame   The chaos game model.
   */
  public FractalController(FractalView fractalView, ChaosGame chaosGame) {
    this.chaosGame = chaosGame;
    if (chaosGame.getDescription() == null) {
      setFractalToSierpinski();
    }

    this.fractalView = fractalView;
    graphicsContext = fractalView.getGraphicsContext2D();
    chaosGame.setWidth((int) fractalView.getWidth());
    chaosGame.setHeight((int) fractalView.getHeight());
    fractalView.draw();

    fractalView.widthProperty().addListener(e -> {
      chaosGame.setWidth((int) fractalView.getWidth());
      fractalView.draw();
    });

    fractalView.heightProperty().addListener(e -> {
      chaosGame.setHeight((int) fractalView.getHeight());
    });

    fractalView.setOnScroll(e -> {
      if (e.getDeltaY() > 0) {
        chaosGame.setScale(chaosGame.getScale() * 1.01f);
      } else {
        chaosGame.setScale(chaosGame.getScale() * 0.99f);
      }
      chaosGame.setScale(chaosGame.getScale());
    });

    fractalView.setOnMouseDragged(e -> {
      chaosGame.setOffset(chaosGame.getCanvas().getOffsetX() + dragStartX - (int) e.getX(), chaosGame.getCanvas().getOffsetY() + dragStartY - (int) e.getY());
      dragStartX = (int) e.getX();
      dragStartY = (int) e.getY();
    });

    fractalView.setOnMousePressed(e -> {
      dragStartX = (int) e.getX();
      dragStartY = (int) e.getY();
    });

  }

  /**
   * <h2>Set fractal to Sierpinski</h2>
   * <p>
   * Sets the fractal to Sierpinski.
   * </p>
   */
  public void setFractalToSierpinski() {
    chaosGame.setCurrentFractalName(FractalName.SIERPINSKI);

    chaosGame.setDescription(new ChaosGameDescription(
        new Vector2D(0, 0),
        new Vector2D(0.7, 0.7),
        List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5))
        )
    ));

  }

  /**
   * <h2>Set fractal to Barnsley Fern</h2>
   * <p>
   * Sets the fractal to Barnsley Fern.
   * </p>
   */
  public void setFractalToBarnsleyFern() {
    chaosGame.setCurrentFractalName(FractalName.BARNSLEY);

    chaosGame.setDescription(new ChaosGameDescription(
        new Vector2D(-2.1820, 0),
        new Vector2D(2.6558, 9.9983),
        List.of(
            new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)),
            new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)),
            new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44))
        )
    ));
  }

  /**
   * <h2>Set fractal to Julia set</h2>
   * <p>
   * Sets the fractal to Julia set.
   * </p>
   */
  public void setFractalToJuliaSet() {
    Complex c = new Complex(-.74543, .11301);
    chaosGame.setDescription(new ChaosGameDescription(
        new Vector2D(-1.6, -1),
        new Vector2D(1.6, 1),
        List.of(
            new JuliaTransform(c, 1),
            new JuliaTransform(c, -1)
        )

    ));
    chaosGame.setCurrentFractalName(FractalName.JULIA);
  }

  /**
   * <h2>Set fractal to Levy curve</h2>
   * <p>
   * Sets the fractal to Levy curve.
   * </p>
   */
  public void setFractalToLevyCurve() {
    chaosGame.setCurrentFractalName(FractalName.LEVY_CURVE);
    List<Transform2D> transforms = new ArrayList<>();

    Transform2D levy1 = new AffineTransform2D(new Matrix2x2(0.5, 0.5, -0.5, 0.5),
        new Vector2D(0, 0));
    Transform2D levy2 = new AffineTransform2D(new Matrix2x2(0.5, -0.5, 0.5, 0.5),
        new Vector2D(-0.5, -0.5));
    transforms.add(levy1);
    transforms.add(levy2);
    Vector2D min = new Vector2D(-1.53, -1.53);
    Vector2D max = new Vector2D(0.53, 0.53);
    chaosGame.setDescription(new ChaosGameDescription(min, max, transforms));

  }

}
