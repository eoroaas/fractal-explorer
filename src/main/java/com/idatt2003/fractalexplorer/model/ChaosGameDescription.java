package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.Transform2D;

import java.util.List;

/**
 * <h2>Chaos Game Description</h2>
 * <p>Class for the chaos game description.</p>
 */
public record ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords, List<Transform2D> transform) {

}
