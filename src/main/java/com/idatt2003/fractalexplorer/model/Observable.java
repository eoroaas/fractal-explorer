package com.idatt2003.fractalexplorer.model;

import java.util.ArrayList;

/**
 * Interface for the Observable
 * This interface is responsible for handling the Observable object
 */
public interface Observable {
  ArrayList<Observer> observers = new ArrayList<>();

  /**
   * Method to add an observer
   *
   * @param observer The observer
   */
  default void addObserver(Observer observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    observers.add(observer);
  }

  /**
   * Method to remove an observer
   *
   * @param observer The observer
   */
  default void removeObserver(Observer observer) {
    observers.remove(observer);
  }

  /**
   * Method to notify the observers
   */
  default void notifyObservers() {
    for (Observer observer : observers) {
      observer.update();
    }
  }

}
