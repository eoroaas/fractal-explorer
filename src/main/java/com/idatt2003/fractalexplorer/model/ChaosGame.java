package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.Transform2D;

import com.idatt2003.fractalexplorer.enums.FractalName;

import java.util.Random;

/**
 * <h2>Chaos Game</h2>
 * <p>Class for the chaos game.</p>
 */
public class ChaosGame implements Observable {

  private final Random random;
  FractalName currentFractal;
  private ChaosGameCanvas canvas;
  private ChaosGameDescription description;
  private Vector2D currentPoint;
  private int width;
  private int height;
  private int offsetX;
  private int offsetY;
  private float scale;
  private int steps = 10000;

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new chaos game with the given description, height, width and current fractal.</p>
   *
   * @param description    The description
   * @param height         The height
   * @param width          The width
   * @param currentFractal The current fractal
   */
  public ChaosGame(ChaosGameDescription description, int height, int width, FractalName currentFractal) {
    this.currentFractal = currentFractal;
    this.random = new Random();
    this.description = description;
    this.width = width;
    this.height = height;
    this.scale = 1;
    if (description != null) {
      this.canvas = new ChaosGameCanvas(height, width, description.minCoords(), description.maxCoords());

    }
    this.currentPoint = new Vector2D(0, 0);
  }

  public int getOffsetX() {
    return offsetX;
  }

  public int getOffsetY() {
    return offsetY;
  }

  public int getHeight() {
    return height;
  }

  /**
   * <h2>Get the height</h2>
   *
   * @return The height
   */
  public void setHeight(int height) {
    this.height = height;
    this.canvas = new ChaosGameCanvas(width, height, description.minCoords(), description.maxCoords());
  }

  /**
   * <h2>Get the scale</h2>
   *
   * @return The scale
   */
  public float getScale() {
    return scale;
  }

  /**
   * <h2>Set the scale</h2>
   *
   * @param scale The scale
   */
  public void setScale(float scale) {
    this.scale = scale;
    this.canvas = new ChaosGameCanvas(width, height, description.minCoords().scale(scale), description.maxCoords().scale(scale), offsetX, offsetY);
    notifyObservers();
  }

  /**
   * <h2>Get the height</h2>
   *
   * @return The height
   */
  public int getSteps() {
    return steps;
  }

  /**
   * <h2>Set the steps</h2>
   *
   * @param steps The steps
   */
  public void setSteps(int steps) {
    this.steps = steps;
    notifyObservers();
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public ChaosGameCanvas getCanvas() {
    return canvas;
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public FractalName getCurrentFractalName() {
    return currentFractal;
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public void setCurrentFractalName(FractalName currentFractal) {
    this.currentFractal = currentFractal;

    notifyObservers();
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public ChaosGameDescription getDescription() {
    return description;
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public void setDescription(ChaosGameDescription description) {
    this.description = description;
    this.canvas = new ChaosGameCanvas(width, height, description.minCoords().scale(scale), description.maxCoords().scale(scale));
    notifyObservers();
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public void setOffset(int offsetX, int offsetY) {
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.canvas = new ChaosGameCanvas(width, height, description.minCoords().scale(scale), description.maxCoords().scale(scale), offsetX, offsetY);
    notifyObservers();
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public void runSteps() {
    int c = 0;
    for (int i = 0; i < steps; i++) {
      int randomIndex = random.nextInt(description.transform().size());
      Transform2D transform2D = description.transform().get(randomIndex);
      currentPoint = transform2D.transform(currentPoint);
      if (canvas.putPixel(currentPoint)) {
        c++;
      }
    }
  }

  /**
   * <h2>Get the current point</h2>
   *
   * @return The current point
   */
  public void setFractalName(FractalName fractalName) {
    this.currentFractal = fractalName;
    notifyObservers();
  }

  public int getWidth() {
    return width;
  }

  /**
   * <h2>Set the width</h2>
   *
   * @param width The width
   */
  public void setWidth(int width) {
    this.width = width;
    this.canvas = new ChaosGameCanvas(width, height, description.minCoords(), description.maxCoords());
  }
}

