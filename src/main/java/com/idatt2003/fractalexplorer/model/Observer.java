package com.idatt2003.fractalexplorer.model;

/**
 * Interface for the Observer
 * This interface is responsible for handling the Observer object
 */
public interface Observer {

  /**
   * Method to update the observer
   */
  void update();

}
