package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.enums.FractalName;
import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;

import java.util.List;

/**
 * <h2>Chaos game description</h2>
 * <p>Class for the chaos game description</p>
 * <p>Currently not implemented yet as there was uncertainty if this would follow the MVC pattern</p>
 */
public class ChaosGameDescriptionFactory {

  /**
   * <h2>Create Sierpinski description</h2>
   * <p>Creates a new Sierpinski description</p>
   *
   * @return The Sierpinski description
   */
  public static ChaosGameDescription createSierpinskiDescription() {
    return new ChaosGameDescription(
        new Vector2D(0, 0),
        new Vector2D(0.7, 0.7),
        List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5))
        )
    );
  }
}
