package com.idatt2003.fractalexplorer.model;

import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;
import com.idatt2003.fractalexplorer.model.math.transformations.AffineTransform2D;

/**
 * <h2>Chaos game canvas</h2>
 *
 * <p>Class for the chaos game canvas</p>
 */
public class ChaosGameCanvas {

  private final int[][] canvas;
  private final AffineTransform2D transformCoordsToIndices;
  private int offsetX;
  private int offsetY;

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new chaos game canvas with the given width, height, min and max coordinates</p>
   *
   * @param width     The width
   * @param height    The height
   * @param minCoords The minimum coordinates
   * @param maxCoords The maximum coordinates
   */
  public ChaosGameCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.offsetX = 0;
    this.offsetY = 0;
    canvas = new int[height][width];
    double a01 = (height - 1) / (minCoords.getX1() - maxCoords.getX1());
    double a10 = (width - 1) / (maxCoords.getX0() - minCoords.getX0());

    Matrix2x2 matrixA = new Matrix2x2(0, a01, a10, 0);
    Vector2D matrixB = new Vector2D(
        ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
        ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0()));

    transformCoordsToIndices = new AffineTransform2D(matrixA, matrixB);

  }

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new chaos game canvas with the given width, height, min and max coordinates, offset x and y</p>
   *
   * @param width     The width
   * @param height    The height
   * @param minCoords The minimum coordinates
   * @param maxCoords The maximum coordinates
   * @param offsetX   The offset x
   * @param offsetY   The offset y
   */
  public ChaosGameCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords, int offsetX, int offsetY) {
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    canvas = new int[height][width];
    double a01 = (height - 1) / (minCoords.getX1() - maxCoords.getX1());
    double a10 = (width - 1) / (maxCoords.getX0() - minCoords.getX0());

    Matrix2x2 matrixA = new Matrix2x2(0, a01, a10, 0);
    Vector2D matrixB = new Vector2D(
        ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
        ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0()));

    transformCoordsToIndices = new AffineTransform2D(matrixA, matrixB);

  }

  /**
   * <h2>Get the offset y</h2>
   *
   * @return The offset y
   */
  public int getOffsetY() {
    return offsetY;
  }

  /**
   * <h2>Set the offset y</h2>
   *
   * @param offsetY The offset y
   */
  public void setOffsetY(int offsetY) {
    this.offsetY = offsetY;
  }

  /**
   * <h2>Get the offset x</h2>
   *
   * @return The offset x
   */
  public int getOffsetX() {
    return offsetX;
  }

  /**
   * <h2>Set the offset x</h2>
   *
   * @param offsetX The offset x
   */
  public void setOffsetX(int offsetX) {
    this.offsetX = offsetX;
  }

  /**
   * <h2>Clear the canvas</h2>
   * <p>Clears the canvas</p>
   */
  public void clearCanvas() {
    for (int i = 0; i < canvas.length; i++) {
      for (int j = 0; j < canvas[0].length; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * <h2>Get the canvas array</h2>
   *
   * @return The canvas array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * <h2>Put pixel</h2>
   * <p>Put a pixel on the canvas at the given point</p>
   *
   * @param point The point
   * @return True if the pixel was put, false if not
   */
  public boolean putPixel(Vector2D point) {
    int cordI = (int) transformCoordsToIndices.transform(point).getX0() - offsetY;
    int cordJ = (int) transformCoordsToIndices.transform(point).getX1() - offsetX;

    try {
      canvas[cordI][cordJ] = 1;
      return true;
    } catch (ArrayIndexOutOfBoundsException e) {
      //points out of bounds are not added to the canvas
      //we then return
      return false;
    }
  }
}
