package com.idatt2003.fractalexplorer.model.math;

/**
 * <h2>2D vector</h2>
 */
public class Vector2D {

  private final double x0; // x-component of the vector
  private final double x1; // y-component of the vector

  /**
   * <h2>Constructor</h2>
   * <p>Constructor for a 2D vector.</p>
   *
   * @param x0 x-component of the vector
   * @param x1 y-component of the vector
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0; // Set the x-component
    this.x1 = x1; // Set the y-component
  }

  /**
   * <h2>Add</h2>
   * <p>Adds another vector to this vector.</p>
   *
   * @param other The vector to add
   * @return The sum of the two vectors
   */
  public Vector2D add(Vector2D other) {
    // Compute the sum of x-components and y-components separately
    double newX0 = x0 + other.getX0(); // Sum of x-components
    double newX1 = x1 + other.getX1(); // Sum of y-components
    // Return a new Vector2D object with the computed sum
    return new Vector2D(newX0, newX1);
  }

  /**
   * <h2>Get the x-component</h2>
   *
   * @return The x-component
   */
  public double getX0() {
    return x0;
  }

  /**
   * <h2>Get the y-component</h2>
   *
   * @return The y-component
   */
  public double getX1() {
    return x1;
  }

  /**
   * <h2>Subtract</h2>
   * <p>Subtracts another vector from this vector.</p>
   *
   * @param other The vector to subtract
   * @return The difference of the two vectors
   */
  public Vector2D subtract(Vector2D other) {
    // Compute the difference of x-components and y-components separately
    double newX0 = x0 - other.getX0(); // Difference of x-components
    double newX1 = x1 - other.getX1(); // Difference of y-components
    // Return a new Vector2D object with the computed difference
    return new Vector2D(newX0, newX1);
  }

  /**
   * <h2>Scale</h2>
   * <p>Scales the vector by a factor.</p>
   *
   * @param factor The factor to scale by
   * @return The scaled vector
   */
  public Vector2D scale(double factor) {
    return new Vector2D(x0 * factor, x1 * factor);

  }

  /**
   * <h2>To string</h2>
   * <p>Converts the vector to a string.</p>
   *
   * @return The string representation of the vector
   */
  @Override
  public String toString() {

    //we need to avoid -0 in the output
    return "Vector2D{" +
        "x0=" + (x0 == -0 ? 0 : x0) +
        ", x1=" + (x1 == -0 ? 0 : x1) +
        '}';
  }
}
