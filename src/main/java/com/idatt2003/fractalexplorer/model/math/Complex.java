package com.idatt2003.fractalexplorer.model.math;

/**
 * <h2>Complex number</h2>
 * <p>Class for complex numbers</p>
 */
public class Complex extends Vector2D {

  private double realPart; // Real part of the complex number
  private double imaginaryPart; // Imaginary part of the complex number

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new complex number with the given real and imaginary parts</p>
   *
   * @param realPart      The real part
   * @param imaginaryPart The imaginary part
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
    this.realPart = realPart;
    this.imaginaryPart = imaginaryPart;
  }

  /**
   * <h2>Subtract</h2>
   * <p>Subtracts another complex number from this complex number</p>
   *
   * @param other The complex number to subtract
   * @return The result of the subtraction
   */
  public Complex subtract(Complex other) {
    double newRealPart = this.realPart - other.realPart;
    double newImaginaryPart = this.imaginaryPart - other.imaginaryPart;
    return new Complex(newRealPart, newImaginaryPart);
  }

  /**
   * <h2>Subtract</h2>
   * <p>Subtracts a vector from this complex number</p>
   * <p>Used for Julia transformations</p>
   *
   * @param vector The vector to subtract
   * @return The result of the subtraction
   */
  public Complex subtract(Vector2D vector) {
    double newRealPart = this.realPart - vector.getX0();
    double newImaginaryPart = this.imaginaryPart - vector.getX1();
    return new Complex(newRealPart, newImaginaryPart);
  }

  /**
   * <h2>negate</h2>
   * <p>Negates the complex number</p>
   * <p>Used for Julia transformations</p>
   *
   * @return The negated complex number
   */
  public Complex negate() {
    return new Complex(-this.realPart, -this.imaginaryPart);
  }

  /**
   * <h2>Square root</h2>
   * <p>Calculates the square root of the complex number</p>
   *
   * @return The square roots of the complex number
   */
  public Complex[] sqrt() {
    double magnitude = Math.sqrt(Math.sqrt(realPart * realPart + imaginaryPart * imaginaryPart));
    double angle = Math.atan2(imaginaryPart, realPart) / 2.0;
    Complex root1 = new Complex(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
    Complex root2 = new Complex(-magnitude * Math.cos(angle), -magnitude * Math.sin(angle));
    return new Complex[]{root1, root2};
  }

  /**
   * <h2>Get the real part</h2>
   *
   * @return The real part
   */
  public double getRealPart() {
    return realPart;
  }

  /**
   * <h2>Set the real part</h2>
   *
   * @param realPart The real part
   */
  public void setRealPart(double realPart) {
    this.realPart = realPart;
  }

  /**
   * <h2>Get the imaginary part</h2>
   *
   * @return The imaginary part
   */
  public double getImaginaryPart() {
    return imaginaryPart;
  }

  /**
   * <h2>Set the imaginary part</h2>
   *
   * @param imaginaryPart The imaginary part
   */
  public void setImaginaryPart(double imaginaryPart) {
    this.imaginaryPart = imaginaryPart;
  }
}