package com.idatt2003.fractalexplorer.model.math.transformations;

import com.idatt2003.fractalexplorer.model.math.Vector2D;

/**
 * Interface for 2D transformations
 */
public interface Transform2D {

  //Interface method for 2D transformations
  //This method takes a 2D vector point as an input and return another 2D vector
  Vector2D transform(Vector2D point);
}
