package com.idatt2003.fractalexplorer.model.math.transformations;

import com.idatt2003.fractalexplorer.model.math.Matrix2x2;
import com.idatt2003.fractalexplorer.model.math.Vector2D;

/**
 * <h2>Affine transformation in 2D</h2>
 * <p>Class for an affine transformation in 2D space.</p>
 */
public class AffineTransform2D implements Transform2D {
  private final Matrix2x2 matrix; //Transformation matrix

  private final Vector2D vector; //Translation vector

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new affine transformation with the given transformation matrix and translation vector.</p>
   *
   * @param matrix The transformation matrix
   * @param vector The translation vector
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix; //Set the transformation matrix
    this.vector = vector; //Set the translation vector
  }

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new affine transformation with the given transformation matrix and zero translation vector.</p>
   *
   * @param point The point
   */
  public Vector2D transform(Vector2D point) {
    //Apply the transformation matrix to the point, then add the translation vector
    return matrix.multiply(point).add(vector);
  }

  /**
   * <h2>Get the transformation matrix</h2>
   *
   * @return The transformation matrix
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * <h2>Get the translation vector</h2>
   *
   * @return The translation vector
   */
  public Vector2D getVector() {
    return vector;
  }
}
