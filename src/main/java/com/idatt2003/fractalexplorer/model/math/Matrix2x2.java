package com.idatt2003.fractalexplorer.model.math;

/**
 * <h2>2x2 Matrix</h2>
 * <p>Class for a 2x2 matrix with methods for matrix multiplication.</p>
 */
public class Matrix2x2 {
  double a00; // Element at row 0, column 0
  double a01; // Element at row 0, column 1
  double a10; // Element at row 1, column 0
  double a11; // Element at row 1, column 1

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new 2x2 matrix with the given elements.</p>
   *
   * @param a00 Element at row 0, column 0
   * @param a01 Element at row 0, column 1
   * @param a10 Element at row 1, column 0
   * @param a11 Element at row 1, column 1
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00; // Set element at row 0, column 0
    this.a01 = a01; // Set element at row 0, column 1
    this.a10 = a10; // Set element at row 1, column 0
    this.a11 = a11; // Set element at row 1, column 1
  }

  /**
   * <h2>Multiply</h2>
   * <p>Multiplies the matrix with the given vector.</p>
   *
   * @param vector The vector to multiply with
   * @return The resulting vector
   */
  public Vector2D multiply(Vector2D vector) {
    // Perform matrix multiplication with the given vector
    double x0 = a00 * vector.getX0() + a01 * vector.getX1(); // Compute the x component
    double x1 = a10 * vector.getX0() + a11 * vector.getX1(); // Compute the y component
    // Return the resulting vector
    return new Vector2D(x0, x1);
  }

  /**
   * <h2>To string</h2>
   * <p>Converts the matrix to a string.</p>
   * <p>Example: Matrix2x2{a00=1.0, a01=0.0, a10=0.0, a11=1.0}</p>
   *
   * @return The string representation of the matrix
   */
  @Override
  public String toString() {
    return "Matrix2x2{" +
        "a00=" + a00 +
        ", a01=" + a01 +
        ", a10=" + a10 +
        ", a11=" + a11 +
        '}';
  }

}
