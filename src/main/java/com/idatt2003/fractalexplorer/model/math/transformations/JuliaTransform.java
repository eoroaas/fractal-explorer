package com.idatt2003.fractalexplorer.model.math.transformations;

import com.idatt2003.fractalexplorer.model.math.Complex;
import com.idatt2003.fractalexplorer.model.math.Vector2D;

import java.util.Random;

/**
 * <h2>Julia transformation in 2D</h2>
 * <p>Class for a Julia transformation in 2D space.</p>
 */
public class JuliaTransform implements Transform2D {

  private final Complex point; // Complex constant representing the Julia set parameter
  private final int sign; // Sign indicating whether to use positive or negative square root

  /**
   * <h2>Constructor</h2>
   * <p>Creates a new Julia transformation with the given complex constant and sign.</p>
   *
   * @param point The complex constant
   * @param sign  The sign
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point; // Set the complex constant
    this.sign = sign; // Set the sign
  }

  /**
   * <h2>Transform a point</h2>
   * <p>Transforms a point using the Julia transformation.</p>
   *
   * @param vector2D The point to transform
   * @return The transformed point
   */
  @Override
  public Vector2D transform(Vector2D vector2D) {
    Complex input = new Complex(vector2D.getX0(), vector2D.getX1());
    Complex subtracted = input.subtract(this.point);
    Complex[] roots = subtracted.sqrt(); // Get both roots

    // Choose which root to use
    Complex chosenRoot;
    if (this.sign != 0) {
      chosenRoot = this.sign > 0 ? roots[0] : roots[1];
    } else {
      chosenRoot = new Random().nextInt(2) == 0 ? roots[0] : roots[1];
    }

    // Convert back to Vector2D and return
    return new Vector2D(chosenRoot.getRealPart(), chosenRoot.getImaginaryPart());
  }
}