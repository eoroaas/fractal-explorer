package com.idatt2003.fractalexplorer.enums;

/**
 * <h2>Fractal names</h2>
 * <p>Enum for the different fractals that can be generated.</p>
 */
public enum FractalName {
  BARNSLEY, SIERPINSKI, LEVY_CURVE, CUSTOM, JULIA
}
