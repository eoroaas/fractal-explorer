package com.idatt2003.fractalexplorer.enums;

/**
 * <h2>Transformation types</h2>
 * <p>Enum for the different transformation types that can be used.</p>
 */
public enum TransformationType {
  AFFINE, JULIA
}
