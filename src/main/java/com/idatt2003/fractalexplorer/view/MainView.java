package com.idatt2003.fractalexplorer.view;

import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.view.pages.ExportModalPage;
import com.idatt2003.fractalexplorer.view.pages.ImportModalPage;
import com.idatt2003.fractalexplorer.view.pages.MainPage;
import com.idatt2003.fractalexplorer.view.pages.SettingsModalPage;
import javafx.scene.layout.StackPane;

/**
 * <h2>Main View</h2>
 * <p>
 * Class for the main view
 * </p>
 * <p>
 * Used to create the main view of the application
 * </p>
 */
public class MainView extends StackPane {
  public MainPage mainPage;
  public SettingsModalPage settingsModalPage;
  public ExportModalPage exportModalPage;

  public ImportModalPage importModalPage;

  public MainView(ChaosGame chaosGame) {
    super();
    mainPage = new MainPage(chaosGame);
    settingsModalPage = new SettingsModalPage(chaosGame);
    exportModalPage = new ExportModalPage();
    importModalPage = new ImportModalPage();
    this.getChildren().addAll(settingsModalPage, exportModalPage, importModalPage, mainPage);
  }
}
