package com.idatt2003.fractalexplorer.view.components;

import com.idatt2003.fractalexplorer.MainApplication;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.girod.javafx.svgimage.SVGImage;
import org.girod.javafx.svgimage.SVGLoader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <h2>Modal</h2>
 * <p>Class for the modal</p>
 * <p>Used to create a modal with a title and a close button</p>
 */
public class Modal extends StackPane {

  private final VBox mainContent = new VBox();

  private final Button xButton = new Button();

  /**
   * <h2>Modal</h2>
   * <p>Constructor for the modal</p>
   *
   * @param titleText The title of the modal
   */
  public Modal(String titleText) {
    super();
    xButton.setAlignment(Pos.TOP_RIGHT);
    xButton.getStyleClass().add("x-button");
    xButton.setStyle("-fx-cursor: hand");
    try {
      SVGImage img = SVGLoader.load(Files.readString(Paths.get(getClass().getResource("x_icon.svg").toURI()), StandardCharsets.UTF_8));
      xButton.setGraphic(img);
    } catch (Exception e) {
      MainApplication.logger.log(System.Logger.Level.DEBUG, "SVG icon not found, likely a bad path.");
      xButton.setText("X");
    }

    StackPane.setAlignment(xButton, Pos.TOP_RIGHT);

    mainContent.setAlignment(Pos.TOP_LEFT);
    mainContent.getStyleClass().add("main-content");
    this.getStylesheets().add(this.getClass().getResource("modal.css").toExternalForm());
    this.getStyleClass().add("modal");
    this.setAlignment(Pos.CENTER);

    Label title = new Label(titleText);
    title.getStyleClass().add("modal-title");
    mainContent.getChildren().add(title);
    this.getChildren().add(mainContent);
    this.getChildren().add(xButton);

  }

  public void addChild(Node node) {
    mainContent.getChildren().add(node);
  }

  /**
   * <h2>Set on exit action</h2>
   * <p>Method to set the action when the modal is closed</p>
   *
   * @param r The action to be performed
   */
  public void setOnExitAction(Runnable r) {

    xButton.setOnAction((e) -> {
      r.run();
    });
  }

}
