package com.idatt2003.fractalexplorer.view.components;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

/**
 * <h2>Select Input</h2>
 * <p>Class for the select input</p>
 * <p>Used to create a select input</p>
 */
public class SelectInput extends VBox {

  private final ComboBox<String> comboBox;

  private final Label errorLabel;

  /**
   * <h2>Select Input</h2>
   * <p>Constructor for the select input</p>
   *
   * @param values The values of the select input
   * @param label  The label of the select input
   */
  public SelectInput(String[] values, String label) {
    super();
    Label label1 = new Label(label);
    this.getChildren().add(label1);
    comboBox = new ComboBox<>();
    comboBox.setPrefWidth(200);
    comboBox.setMinWidth(200);
    this.getStylesheets().add(this.getClass().getResource("input.css").toExternalForm());
    comboBox.getStyleClass().add("select-input");
    errorLabel = new Label();
    errorLabel.getStyleClass().add("error-label");

    for (String value : values) {
      comboBox.getItems().add(value);
    }
    this.getChildren().add(comboBox);
    this.getChildren().add(errorLabel);
  }

  /**
   * <h2>Set on select</h2>
   * <p>Method for setting the on select action</p>
   * <p>Used to set the action that should be performed when a value is selected</p>
   *
   * @param consumer The action to be performed
   */
  public void setOnSelect(Consumer<String> consumer) {
    comboBox.setOnAction(e -> {
      if (comboBox.getValue() != null) {
        consumer.accept(comboBox.getValue());
      }
    });
  }

  /**
   * <h2>Set error</h2>
   * <p>Method for setting the error</p>
   * <p>Used to set the error message of the select input</p>
   *
   * @param error The error message
   */
  public void setError(String error) {
    errorLabel.setText(error);
    this.getStyleClass().add("error");

  }

  /**
   * <h2>Remove error</h2>
   * <p>Method for removing the error</p>
   * <p>Used to remove the error message of the select input</p>
   */
  public void removeError() {
    errorLabel.setText("");
    this.getStyleClass().remove("error");
  }

  /**
   * <h2>Set values</h2>
   * <p>Method for setting the values</p>
   * <p>Used to set the values of the select input</p>
   *
   * @param values The values
   */
  public void setValues(String[] values) {
    comboBox.getItems().clear();
    for (String value : values) {
      comboBox.getItems().add(value);
    }
  }

  /**
   * <h2>Get selected</h2>
   * <p>Method for getting the selected value</p>
   * <p>Used to get the selected value of the select input</p>
   *
   * @return The selected value
   */
  public String getSelected() {
    return comboBox.getValue();
  }

  /**
   * <h2>Set selected</h2>
   * <p>Method for setting the selected value</p>
   * <p>Used to set the selected value of the select input</p>
   *
   * @param value The value
   */
  public void setSelected(String value) {
    comboBox.setValue(value);
  }

}
