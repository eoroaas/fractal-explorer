package com.idatt2003.fractalexplorer.view.components;

import javafx.scene.control.Button;

/**
 * <h2>Custom Button</h2>
 * <p>Class for the custom button</p>
 * <p>Used to create custom buttons with a custom style</p>
 */
public class CustomButton extends Button {
  public CustomButton(String text) {

    super(text);
    this.getStylesheets().add(this.getClass().getResource("button.css").toExternalForm());

    this.getStyleClass().add("button");
  }
}
