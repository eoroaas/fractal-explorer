package com.idatt2003.fractalexplorer.view.components;

import com.idatt2003.fractalexplorer.MainApplication;
import com.idatt2003.fractalexplorer.exceptions.BadInputException;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * <h2>Text Input</h2>
 * <p>Class for the text input</p>
 * <p>Used to create a text input</p>
 */
public class TextInput extends VBox {

  Label errorLabel;

  /**
   * <h2>Text Input</h2>
   * <p>Constructor for the text input</p>
   *
   * @param label The label of the text input
   */
  public TextInput(String label) {
    super();
    Label label1 = new Label(label);

    label1.getStyleClass().add("input-label");
    errorLabel = new Label();
    errorLabel.getStyleClass().add("error-label");

    this.getChildren().add(label1);
    TextField textField = new TextField();
    textField.getStyleClass().add("text-input");
    this.getStylesheets().add(this.getClass().getResource("input.css").toExternalForm());

    textField.setMaxWidth(100);
    this.getChildren().add(textField);
    this.getChildren().add(errorLabel);
  }

  /**
   * <h2>Try get double</h2>
   * <p>Method for trying to get a double</p>
   * <p>Used to try to get a double from the text input</p>
   *
   * @return The double
   * @throws BadInputException If the input is invalid
   */
  public Double tryGetDouble() throws BadInputException {
    try {
      Double val = Double.parseDouble(getValue());
      removeError();
      return val;
    } catch (NumberFormatException e) {
      MainApplication.logger.log(System.Logger.Level.ERROR, "Invalid number");
      setError("Invalid number");
      throw new BadInputException("Invalid number");
    }
  }

  /**
   * <h2>Get value</h2>
   * <p>Method for getting the value</p>
   * <p>Used to get the value of the text input</p>
   *
   * @return The value
   */
  public String getValue() {
    return ((TextField) this.getChildren().get(1)).getText();
  }

  /**
   * <h2>Set value</h2>
   * <p>Method for setting the value</p>
   * <p>Used to set the value of the text input</p>
   *
   * @param value The value
   */
  public void setValue(String value) {
    ((TextField) this.getChildren().get(1)).setText(value);
  }

  /**
   * <h2>Remove error</h2>
   * <p>Method for removing the error</p>
   * <p>Used to remove the error message of the text input</p>
   */
  public void removeError() {
    errorLabel.setText("");
    this.getStyleClass().remove("error");
  }

  /**
   * <h2>Set error</h2>
   * <p>Method for setting the error</p>
   * <p>Used to set the error message of the text input</p>
   *
   * @param error The error message
   */
  public void setError(String error) {
    errorLabel.setText(error);
    this.getStyleClass().add("error");

  }
}
