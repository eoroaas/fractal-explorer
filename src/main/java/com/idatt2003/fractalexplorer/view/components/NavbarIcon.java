package com.idatt2003.fractalexplorer.view.components;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import org.girod.javafx.svgimage.SVGImage;
import org.girod.javafx.svgimage.SVGLoader;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <h2>NavbarIcon</h2>
 * <p>Class for the navbar icon</p>
 * <p>Used to create a navbar icon</p>
 */
public class NavbarIcon extends Button {
  public NavbarIcon(String filename, String title) {
    super();
    Tooltip tooltip = new Tooltip(title);
    tooltip.getStyleClass().add("navbar-tooltip");
    this.setTooltip(tooltip);
    this.getStylesheets().add(this.getClass().getResource("navbar.css").toExternalForm());
    this.getStyleClass().add("navbar-icon");
    try {
      SVGImage img = SVGLoader.load(Files.readString(Paths.get(getClass().getResource(filename).toURI()), StandardCharsets.UTF_8));
      this.setGraphic(img);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
