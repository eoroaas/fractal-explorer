package com.idatt2003.fractalexplorer.view.components;

import com.idatt2003.fractalexplorer.model.Observer;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.model.ChaosGameCanvas;
import javafx.scene.canvas.Canvas;

/**
 * <h2>Fractal View</h2>
 * <p>Class for the fractal view</p>
 * <p>Used to create a fractal view</p>
 */
public class FractalView extends Canvas implements Observer {

  private final ChaosGame chaosGame;

  public FractalView(ChaosGame chaosGame) {
    super();
    this.chaosGame = chaosGame;

    this.setWidth(1000);
    this.setHeight(600);

  }

  @Override
  public void update() {
    draw();

  }

  /**
   * <h2>Draw</h2>
   * <p>Draws the fractal</p>
   */
  public void draw() {
    this.getGraphicsContext2D().clearRect(0, 0, this.getWidth(), this.getHeight());

    chaosGame.getCanvas().clearCanvas();
    chaosGame.runSteps();
    ChaosGameCanvas canvas = chaosGame.getCanvas();

    int[][] canvasPixels = canvas.getCanvasArray();
    for (int y = 0; y < canvasPixels.length - 1; y++) {
      for (int x = 0; x < canvasPixels[0].length - 1; x++) {
        if (canvasPixels[y][x] == 1) {
          this.getGraphicsContext2D().fillRect(x, y, 2, 2);
        }
      }
    }
  }

}
