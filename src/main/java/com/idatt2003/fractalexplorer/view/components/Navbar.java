package com.idatt2003.fractalexplorer.view.components;

import javafx.scene.layout.HBox;

/**
 * <h2>Navbar</h2>
 * <p>Class for the navbar</p>
 * <p>Used to create a navbar</p>
 */
public class Navbar extends HBox {

  public NavbarIcon settingsButton = new NavbarIcon("cog.svg", "Settings");
  public NavbarIcon exportButton = new NavbarIcon("file_export.svg", "Create and export fractal to file");

  public NavbarIcon importButton = new NavbarIcon("file.svg", "Import fractal from file");

  /**
   * <h2>Navbar</h2>
   * <p>Constructor for the navbar</p>
   */
  public Navbar() {
    super();
    this.setAlignment(javafx.geometry.Pos.BOTTOM_RIGHT);
    this.getStylesheets().add(this.getClass().getResource("navbar.css").toExternalForm());
    this.getStyleClass().add("container");

    this.getChildren().addAll(settingsButton, exportButton, importButton);
  }
}
