package com.idatt2003.fractalexplorer.view.components;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

/**
 * <h2>Julia form</h2>
 * <p>Class for the Julia form</p>
 * <p>Used in the menu where you can create custom Julia sets</p>
 */
public class JuliaForm extends VBox {

  TextInput realPart;

  TextInput imaginaryPart;

  /**
   * <h2>Julia form</h2>
   * <p>Constructor for the Julia form</p>
   */
  public JuliaForm() {
    super();
    this.setAlignment(Pos.TOP_LEFT);

    this.setSpacing(8);

    realPart = new TextInput("Real part");

    imaginaryPart = new TextInput("Imaginary part");
    this.getChildren().add(realPart);
    this.getChildren().add(imaginaryPart);
  }

  public TextInput getImaginaryPart() {
    return imaginaryPart;
  }

  public TextInput getRealPart() {
    return realPart;
  }
}
