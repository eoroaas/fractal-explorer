package com.idatt2003.fractalexplorer.view.components;

import javafx.scene.layout.StackPane;

import java.util.function.Function;

/**
 * <h2>Modal Page</h2>
 * <p>Class for the modal page</p>
 * <p>Used to create a modal page with a title and a close button</p>
 */
public class ModalPage extends StackPane {
  private final Modal modal;

  /**
   * <h2>Modal Page</h2>
   * <p>Constructor for the modal page</p>
   *
   * @param titleText The title of the modal
   */
  public ModalPage(String titleText) {
    super();
    this.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5); -fx-padding: 24px;");
    modal = new Modal(titleText);
    StackPane.setAlignment(modal, javafx.geometry.Pos.CENTER);
    this.getChildren().add(modal);

  }

  /**
   * <h2>On exit action</h2>
   * <p>Method for setting the on exit action</p>
   * <p>Used to set the action that should be performed when the modal is closed</p>
   *
   * @param action The action to be performed
   */
  public void setOnExitAction(Runnable action) {
    this.setOnMouseClicked(e -> action.run());
    modal.setOnExitAction(action);
  }

  /**
   * <h2>Hide</h2>
   * <p>Hides the modal</p>
   * <p>Used to hide the modal</p>
   * <p>Does not remove the modal from the scene</p>
   */
  public void hide() {
    this.toBack();
  }

  /**
   * <h2>Get modal</h2>
   * <p>Method for getting the modal</p>
   * <p>Used to get the modal</p>
   *
   * @return The modal
   */
  public Modal getModal() {
    return modal;
  }
}
