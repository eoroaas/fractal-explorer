package com.idatt2003.fractalexplorer.view.components;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

/**
 * <h2>Affine form</h2>
 * <p>Class for the affine form</p>
 * <p>Used in the menu where you can create custom affine transformations</p>
 */
public class AffineForm extends VBox {

  VBox rows;

  public AffineForm() {
    super();
    this.setAlignment(javafx.geometry.Pos.TOP_LEFT);
    this.setSpacing(8);
    CustomButton button = new CustomButton("Add row");

    rows = new VBox();
    button.setOnAction((e) -> {
      rows.getChildren().add(createRow());
    });
    rows.getChildren().add(createRow());
    this.getChildren().add(rows);

    this.getChildren().add(button);
  }

  HBox createRow() {
    HBox row = new HBox();
    row.getChildren().add(new TextInput("a00"));
    row.getChildren().add(new TextInput("a01"));
    row.getChildren().add(new TextInput("a10"));
    row.getChildren().add(new TextInput("a11"));
    row.getChildren().add(new TextInput("b0"));
    row.getChildren().add(new TextInput("b1"));
    CustomButton removeButton = new CustomButton("Remove");
    removeButton.setMinWidth(100);
    removeButton.setAlignment(javafx.geometry.Pos.BOTTOM_RIGHT);
    removeButton.setOnAction((e) -> {
      rows.getChildren().remove(row);
    });
    row.setAlignment(Pos.BOTTOM_LEFT);
    row.getChildren().add(removeButton);

    return row;
  }

  public VBox getRows() {
    return rows;
  }
}
