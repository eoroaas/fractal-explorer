package com.idatt2003.fractalexplorer.view.pages;

import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.view.components.FractalView;
import com.idatt2003.fractalexplorer.view.components.Navbar;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;

/**
 * <h2>Main Page</h2>
 * <p>
 * Class for the main page
 * </p>
 * <p>
 * Used to create the main page of the application
 * </p>
 */
public class MainPage extends StackPane {
  private final Navbar navbar;
  public FractalView fractalView;

  public MainPage(ChaosGame chaosGame) {
    super();
    this.setStyle("-fx-background-color: rgb(255,255,255);");
    navbar = new Navbar();

    fractalView = new FractalView(chaosGame);
    this.heightProperty().addListener((observable, oldValue, newValue) -> {
      fractalView.setHeight(newValue.doubleValue());
    });
    this.widthProperty().addListener((observable, oldValue, newValue) -> {
      fractalView.setWidth(newValue.doubleValue());
    });
    this.getChildren().addAll(fractalView, navbar);

    this.setAlignment(Pos.BOTTOM_RIGHT);
  }

  public Navbar getNavbar() {
    return navbar;
  }

}
