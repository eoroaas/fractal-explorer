package com.idatt2003.fractalexplorer.view.pages;

import com.idatt2003.fractalexplorer.model.Observer;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.view.components.CustomButton;
import com.idatt2003.fractalexplorer.view.components.ModalPage;
import com.idatt2003.fractalexplorer.view.components.SelectInput;
import com.idatt2003.fractalexplorer.view.components.TextInput;

/**
 * <h2>Settings Modal Page</h2>
 * <p>
 * Class for the settings modal page
 * </p>
 * <p>
 * Used to create a modal page for the settings of the fractal
 * </p>
 */
public class SettingsModalPage extends ModalPage implements Observer {

  private final SelectInput fractalType;

  private final ChaosGame chaosGame;

  private final TextInput iterations;
  private final CustomButton saveButton;

  public SettingsModalPage(ChaosGame chaosGame) {
    super("Settings");
    this.chaosGame = chaosGame;
    fractalType = new SelectInput(new String[]{}, "Fractal type");
    this.getModal().addChild(fractalType);
    iterations = new TextInput("Number of iterations");
    this.getModal().addChild(iterations);
    saveButton = new CustomButton("Save");
    this.getModal().addChild(saveButton);
  }

  public TextInput getIterationsInput() {
    return iterations;
  }

  public TextInput getIterations() {
    return iterations;
  }

  public String getIterationsValue() {
    return iterations.getValue();
  }

  public SelectInput getSelectFractalType() {
    return fractalType;
  }

  public void setOnSave(Runnable runnable) {
    saveButton.setOnAction((event) -> {
      runnable.run();
    });
  }

  @Override
  public void update() {
    fractalType.setSelected(chaosGame.getCurrentFractalName().name());
    iterations.setValue(String.valueOf(chaosGame.getSteps()));
  }

}
