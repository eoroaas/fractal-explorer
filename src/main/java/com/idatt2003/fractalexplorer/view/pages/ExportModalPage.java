package com.idatt2003.fractalexplorer.view.pages;

import com.idatt2003.fractalexplorer.view.components.*;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

/**
 * <h2>Export Modal Page</h2>
 * <p>
 * Class for the export modal page
 * </p>
 * <p>
 * Used to create a modal page for exporting a fractal to a file
 * </p>
 */
public class ExportModalPage extends ModalPage {

  Consumer<Void> onApply;
  Consumer<Void> onExport;

  SelectInput fractalType;

  JuliaForm juliaForm;
  AffineForm affineForm;

  TextInput minY;
  TextInput minX;
  TextInput maxY;
  TextInput maxX;

  public ExportModalPage() {
    super("Create fractal");
    fractalType = new SelectInput(new String[]{"Affine", "Julia"}, "Fractal type");
    this.getModal().addChild(fractalType);

    HBox minVector = new HBox();
    minVector.setPadding(new Insets(8, 0, 8, 0));
    minVector.setSpacing(4);
    minX = new TextInput("Min x");
    minVector.getChildren().add(minX);
    minY = new TextInput("Min y");
    minVector.getChildren().add(minY);
    this.getModal().addChild(minVector);

    HBox maxVector = new HBox();
    maxVector.setPadding(new Insets(8, 0, 8, 0));
    maxVector.setSpacing(4);
    maxX = new TextInput("Max x");
    maxVector.getChildren().add(maxX);
    maxY = new TextInput("Max y");

    maxVector.getChildren().add(maxY);
    this.getModal().addChild(maxVector);

    VBox container = new VBox();
    this.getModal().addChild(container);

    juliaForm = new JuliaForm();

    affineForm = new AffineForm();

    fractalType.setOnSelect((String selected) -> {
      if (selected.equals("Julia")) {
        container.getChildren().clear();
        container.getChildren().add(juliaForm);
      } else {

        container.getChildren().clear();
        container.getChildren().add(affineForm);
      }
    });

    CustomButton applyButton = new CustomButton("Apply");
    CustomButton exportButton = new CustomButton("Export to file");
    applyButton.setOnAction((e) -> {
      onApply.accept(null);
    });
    exportButton.setOnAction((e) -> {
      onExport.accept(null);
    });

    this.getModal().addChild(applyButton);
    this.getModal().addChild(exportButton);
  }

  public TextInput getMinX() {
    return minX;
  }

  public TextInput getMinY() {
    return minY;
  }

  public TextInput getMaxY() {
    return maxY;
  }

  public TextInput getMaxX() {
    return maxX;
  }

  public JuliaForm getJuliaForm() {
    return juliaForm;
  }

  public AffineForm getAffineForm() {
    return affineForm;
  }

  public SelectInput getFractalType() {
    return fractalType;
  }

  public void setOnApply(Consumer<Void> onApply) {
    this.onApply = onApply;
  }

  public void setOnExport(Consumer<Void> onExport) {
    this.onExport = onExport;
  }
}
