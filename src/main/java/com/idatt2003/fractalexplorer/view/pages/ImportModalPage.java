package com.idatt2003.fractalexplorer.view.pages;

import com.idatt2003.fractalexplorer.view.components.CustomButton;
import com.idatt2003.fractalexplorer.view.components.ModalPage;
import com.idatt2003.fractalexplorer.view.components.SelectInput;
import com.idatt2003.fractalexplorer.view.components.TextInput;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.function.Consumer;

/**
 * <h2>Import Modal Page</h2>
 * <p>
 * Class for the import modal page
 * </p>
 * <p>
 * Used to create a modal page for importing a fractal from a file
 * </p>
 */
public class ImportModalPage extends ModalPage {

  CustomButton openFileButton;

  Consumer<File> onFileChoosen;

  public ImportModalPage() {
    super("Import from file");

    //select file from file system using file picker
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));

    openFileButton = new CustomButton("Open file");
    openFileButton.setOnAction((event) -> {
      File file = fileChooser.showOpenDialog(this.getScene().getWindow());
      if (onFileChoosen != null && file != null) {
        onFileChoosen.accept(file);
      }
    });
    this.getModal().addChild(openFileButton);
  }

  public void setOnFileChoosen(Consumer<File> onFileChoosen) {
    this.onFileChoosen = onFileChoosen;
  }
}