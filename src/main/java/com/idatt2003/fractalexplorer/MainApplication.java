package com.idatt2003.fractalexplorer;

import com.idatt2003.fractalexplorer.controller.*;
import com.idatt2003.fractalexplorer.model.ChaosGame;
import com.idatt2003.fractalexplorer.view.MainView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * <h2>Main Application</h2>
 * <p>
 * Class for the main application
 * </p>
 * <p>
 * Used to create the main application
 * </p>
 */
public class MainApplication extends Application {
  public static System.Logger logger = System.getLogger(MainApplication.class.getName());

  public static void main(String[] args) {
    launch();
  }

  @Override
  public void start(Stage stage) throws IOException {
    ChaosGame chaosGame = new ChaosGame(null, 400, 800, null);
    MainView mainView = new MainView(chaosGame);

    logger.log(System.Logger.Level.INFO, "Starting application");

    NavigationController navigationController = new NavigationController(mainView);
    FractalController fractalController = new FractalController(mainView.mainPage.fractalView, chaosGame);
    chaosGame.addObserver(mainView.settingsModalPage);
    chaosGame.addObserver(mainView.mainPage.fractalView);
    SettingsController settingsController = new SettingsController(mainView.settingsModalPage, chaosGame, fractalController);
    ExportController exportController = new ExportController(mainView.exportModalPage, chaosGame);
    ImportController importController = new ImportController(mainView.importModalPage, chaosGame);
    chaosGame.notifyObservers();
    Scene scene = new Scene(mainView, 1000, 600);
    stage.setTitle("Fractal Explorer");
    stage.setScene(scene);
    stage.show();
  }
}