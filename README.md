# IDATT2003_ChaosGame

<strong>STUDENT NAME</strong> = Embret Olav Rasmussen Roås <br>
<strong>STUDENT ID</strong> = 10056 <br>
<strong>STUDENT NAME</strong> = Sander Rusten Berge <br>
<strong>STUDENT ID</strong> = 10049

## Project description

<p> This project is a part of the class <strong>IDATT2003 - Programmering 2  at NTNU.</strong></p>

The project is a desktop application written in Java using the javafx
library. The purpose of the applicaiton is to visualize different fractals
using the "chaos game" method. You can choose between different fractals,
or build your own. The application also has a feature to save the fractals
and load saved fractals. You can explore the fractal by dragging or scrolling over the fractal.

## Link to repository

The repository can be found [here](https://gitlab.stud.idi.ntnu.no/eoroaas/fractal-explorer).

## Installation and running the project

1. To run the project you need to have Java 20 or newer installed on your computer.
2. You need to have maven 4.0.0 or greater installed on your computer.
3. Clone the repository to your computer.
4. Open the project in the terminal and run the command `mvn clean install`.
5. Run the project using the command `mvn javafx:run`.
6. Enjoy the application.

## How to run the tests

1. Open the project in the terminal.
2. Run the command `mvn test` to run all the tests in the project.


